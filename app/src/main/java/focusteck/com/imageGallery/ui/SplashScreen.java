package focusteck.com.imageGallery.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import focusteck.com.imageGallery.R;

/**
 * Launcher activity for Punch Agency Android sample code
 * It waits for 2 seconds and then loads GridViewActivity
 */
public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);
        start(2000);
    }

    /**
     * handler method to start GridViewActivity after 2 Seconds using postDelay
     *
     * @param delay time delay in Milliseconds for starting new activity
     */
    private void start(int delay) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, GridViewActivity.class));
                finish();// Finish this activity after new activity is started

            }
        }, delay);
    }

}
