package focusteck.com.imageGallery.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import focusteck.com.imageGallery.R;
import focusteck.com.imageGallery.Utilities.Utils;
import focusteck.com.imageGallery.adapters.ViewPagerAdapter;

/**
 * Activity containing ViewPager that will load all images and will display current selected image
 */
public class ImageDetailActivity extends FragmentActivity implements View.OnClickListener {
    public static final String EXTRA_IMAGE = "extra_image";

    private ViewPagerAdapter mAdapter;
    private ViewPager mPager;
    private List<String> imagesList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_detail_pager);
        imagesList = Utils.getImagesList();
        // Set up ViewPager and backing adapter
        mAdapter = new ViewPagerAdapter(getSupportFragmentManager(), imagesList);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(2);

        // Set up activity to go full screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Set the current item based on the extra passed in to this activity
        final int extraCurrentItem = getIntent().getIntExtra(EXTRA_IMAGE, -1);
        if (extraCurrentItem != -1) {
            mPager.setCurrentItem(extraCurrentItem);
        } else Toast.makeText(this, "Incorrect item index.", Toast.LENGTH_SHORT).show();
    }


    /**
     * Set on the ImageView in the ViewPager children fragments, to enable/disable low profile mode
     * when the ImageView is touched.
     */
    @Override
    public void onClick(View v) {
        final int vis = mPager.getSystemUiVisibility();
        if ((vis & View.SYSTEM_UI_FLAG_LOW_PROFILE) != 0) {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        } else {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
    }
}