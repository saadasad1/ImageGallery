package focusteck.com.imageGallery.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import focusteck.com.imageGallery.R;
import focusteck.com.imageGallery.Utilities.Utils;
import focusteck.com.imageGallery.adapters.GridImageAdapter;

/**
 * The main fragment that powers the GridViewActivity.
 * We load images from List and populate that list into GridView using GridImageAdapter
 */
public class ImageGridFragment extends Fragment implements AdapterView.OnItemClickListener {
    private GridImageAdapter mAdapter;
    private List<String> imagesList = new ArrayList<>();

    /**
     * Empty constructor as per the Fragment documentation
     */
    public ImageGridFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imagesList = Utils.getImagesList();
        mAdapter = new GridImageAdapter(getActivity(), imagesList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.gridview_fragment, container, false);
        final GridView mGridView = (GridView) v.findViewById(R.id.gridView);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
        return v;
    }

    /**
     * ItemClickListener implementation for GridView
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        final Intent i = new Intent(getActivity(), ImageDetailActivity.class);
        i.putExtra(ImageDetailActivity.EXTRA_IMAGE, position);
        startActivity(i);
    }


}
