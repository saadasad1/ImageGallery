package focusteck.com.imageGallery.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import focusteck.com.imageGallery.R;
import focusteck.com.imageGallery.adapters.ViewPagerAdapter;

/**
 * Fragment to show full screen image, child of Viewpager
 */
public class FullScreenImageFragment extends Fragment {
    private String imageUrl = "";

    /**
     * Empty constructor as per the Fragment documentation
     */
    public FullScreenImageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get data from arguments
        imageUrl = getArguments() != null ? getArguments().getString(ViewPagerAdapter.IMAGE) : "";

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fullscreen_image, container, false);
        ImageView mImageView;
        mImageView = (ImageView) v.findViewById(R.id.imageView);
        Picasso.with(getActivity()).load(imageUrl).into(mImageView);
        return v;
    }
}
