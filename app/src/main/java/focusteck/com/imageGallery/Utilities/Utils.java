package focusteck.com.imageGallery.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Utilities class
 */
public class Utils {
    /**
     * populate list of images with http urls
     */
    public static List<String> getImagesList() {
        List<String> imagesList = new ArrayList<>();
        imagesList.add("http://www.planwallpaper.com/static/images/wallpapers-7020-7277-hd-wallpapers.jpg");
        imagesList.add("http://www.planwallpaper.com/static/images/HD-Wallpapers1.jpeg");
        imagesList.add("http://intrawallpaper.com/wp-content/uploads/wallpapers-hd-desktop-wallpapers-free-online-awesome-wallpapers-553869157.jpg");
        imagesList.add("http://cdn.wonderfulengineering.com/wp-content/uploads/2014/09/new-wallpaper-14.jpg");
        imagesList.add("http://www.planwallpaper.com/static/images/4k_game_wallpaper_25.jpg");
        imagesList.add("http://i.imgur.com/DvpvklR.png");
        return imagesList;
    }
}
