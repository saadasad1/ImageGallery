package focusteck.com.imageGallery.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import focusteck.com.imageGallery.ui.FullScreenImageFragment;

/**
 * The main adapter that backs the ViewPager. A subclass of FragmentStatePagerAdapter as there
 * could be a large number of items in the ViewPager and we don't want to retain them all in
 * memory at once but create/destroy them on the fly.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<String> imagesList = new ArrayList<>();
    public static String IMAGE = "image";

    /**
     * Parametrized constructor or ViewPager
     *
     * @param fm         Fragment manager of parent Fragment
     * @param imagesList list of image http urls
     */
    public ViewPagerAdapter(FragmentManager fm, List<String> imagesList) {
        super(fm);
        this.imagesList = imagesList;

    }

    @Override
    public int getCount() {
        return imagesList.size();
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString(IMAGE, imagesList.get(position));
        // set Fragment class Arguments
        FullScreenImageFragment fragment = new FullScreenImageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }
}
