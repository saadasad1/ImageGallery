package focusteck.com.imageGallery.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import focusteck.com.imageGallery.R;

/**
 * Custom adapter to display Images in GridView
 */
public class GridImageAdapter extends BaseAdapter {
    private List<String> Images = new ArrayList<>();
    private final Context mContext;

    /**
     * Parametrized constructor of GridImageAdapter
     *
     * @param context    Context of Class having this gridView
     * @param imagesList List of images http urls
     */
    public GridImageAdapter(Context context, List<String> imagesList) {
        super();
        this.mContext = context;
        this.Images = imagesList;
    }

    @Override
    public int getCount() {
        return Images.size();
    }

    @Override
    public Object getItem(int position) {
        return Images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        ViewHolderItem viewHolder;
        if (convertView == null) { // Check if view is new, in that case create new view and set viewHolder on it
            convertView = LayoutInflater.from(mContext).inflate(R.layout.gridview_item, container, false);
            viewHolder = new ViewHolderItem(convertView);
            convertView.setTag(viewHolder); // set Tag for recycling views
        } else { // recycle viewHolder by using view tag
            viewHolder = (ViewHolderItem) convertView.getTag();
        }
        viewHolder.build(Images.get(position), mContext);
        return convertView;
    }

    /**
     * Class that will hold views within, it will be used to create and recycle views
     */
    private class ViewHolderItem {
        ImageView imageView;

        /**
         * ViewHolder constructor
         *
         * @param convertView parent item view of GridView in which all views reside
         */
        private ViewHolderItem(View convertView) {
            imageView = (ImageView) convertView.findViewById(R.id.imageView);
        }

        /**
         * ViewHolder pattern build method, this method populates data into views
         *
         * @param context  context of the Fragment
         * @param imageUrl http url of the image to load into ImageView
         */
        void build(String imageUrl, Context context) {
            Picasso.with(context).load(imageUrl).resize(500, 500)
                    .centerCrop().into(imageView);
        }

    }

}